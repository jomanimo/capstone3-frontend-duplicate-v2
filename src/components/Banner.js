import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import BannerCSS from '../BannerCSS.css';

export default function Banner({ data }) {
    const { title, description, destination, label, content } = data;
    console.log("HERE IS THE DATA")
    console.log(data)
    return (
    <Row className="justify-content-center align-items-center banner">
    <Col className="text-center">
        <h1>{title}</h1>
        <div>{description}</div>
        <div>{content}</div>
        <Button varaint = "primary" className="button-custom" as= {Link} to ={destination}>{label}</Button>
    </Col>
    </Row>
    )
}
