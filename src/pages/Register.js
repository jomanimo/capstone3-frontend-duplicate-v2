import { useState, useEffect, useContext } from "react";

import { Navigate, useNavigate } from "react-router-dom";
import {Form, Button} from "react-bootstrap";
import Swal from "sweetalert2";
import RegisterCSS from "../RegisterCSS.css";
import UserContext from "../UserContext";

export default function Register(){
    
    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    // create state hooks to store the values of the input fields
    const [firstName, setFName] = useState('');
    const [lastName, setLName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');


    const [isActive, setIsActive] = useState(false);

    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password);
    console.log(password2);

    useEffect(() =>{

        if((firstName !== '' && lastName !=='' && email !== '' && mobileNo !== '' && password !== '' && password2 !=='') && (password === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password, password2])

    function registerUser(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Kindly provide another email to complete the registration."
                })
            }
            else{
                fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
                    method: "POST",
                    headers:{ "Content-Type": "application/json" },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password2,
                        mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to Capstone3 Store!"
                        });

                        setFName('');
                        setLName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');

                        navigate("/login");
                    }
                    else{
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        });

                    }
                })


            }
        })

        


    }

    return(
        (user.id !== null)
        ?
        <Navigate to="/products" />
        :
        <div className="form-container">
        <Form onSubmit = {(event) => registerUser(event)}>
            <Form.Group controlId="userFirstName">
            <Form.Label style={{ color: 'white', paddingTop: '20px'}}>First Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter your first name"
                        value = {firstName}
                        onChange = {event => setFName(event.target.value)}
                        required
                        className = "form-input"
                        />
            </Form.Group>

            <Form.Group controlId="userLastName">
            <Form.Label style={{ color: 'white', paddingTop: '20px'}}>Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter your last name"
                        value = {lastName}
                        onChange = {event => setLName(event.target.value)}
                        required
                        className = "form-input"
                        />
            </Form.Group>

            <Form.Group controlId="userEmail">
            <Form.Label style={{ color: 'white', paddingTop: '20px'}}>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                        className = "form-input"
                            />
                    <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                    </Form.Text>
            </Form.Group>

            <Form.Group controlId="UserMobileNumber">
            <Form.Label style={{ color: 'white', paddingTop: '20px'}}>Mobile Number</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter your mobile number"
                        value = {mobileNo}
                        onChange = {event => setMobileNo(event.target.value)}
                        required
                        className = "form-input"
                        />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label style={{ color: 'white', paddingTop: '20px'}}>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value = {password}
                        onChange = {event => setPassword1(event.target.value)}
                        required
                        className = "form-input"
                        />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label style={{ color: 'white', paddingTop: '20px'}}>Verify Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Verify Password"
                        value = {password2}
                        onChange = {event => setPassword2(event.target.value)}
                        required
                        className = "form-input"
                        />
            </Form.Group>
            { isActive ?
                <Button  className="btn" variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button className="btn" variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
                }
        </Form>
        </div>
    )
}