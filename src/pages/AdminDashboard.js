import {useContext, useEffect, useState} from "react";

import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";
import AdminDashboardCSS from "../AdminDashboardCSS.css";


import UserContext from "../UserContext";

export default function AdminDashboard(){

	const { user } = useContext(UserContext);
    console.log(user);
	// Create allProducts state to contain the products from the response of our fetch data.
	const [allProducts, setAllProducts] = useState([]);
    const [allUsers, setAllUsers] = useState([])

	// State hooks to store the values of the input fields for our modal.
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// To control the add product modal pop out
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal

	// To control the edit product modal pop out
	// We have passed a parameter from the edit button so we can retrieve a specific product and bind it with our input fields.
	const openEdit = (id) => {
		setProductId(id);

		// Getting a specific product to pass on the edit modal
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the product states for editing
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
        setName('');
        setDescription('');
        setPrice(0);
        setStocks(0);

		setShowEdit(false);
	};

	// [SECTION] To view all product in the database (active & inactive)
	// fetchData() function to get all the active/inactive products.
	const fetchData = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/products/`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id}>
						<td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{product._id}</td>
						<td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{product.name}</td>
						<td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{product.description}</td>
						<td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{product.price}</td>
						<td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{product.stocks}</td>
						<td td style={{ color: product.isActive ? 'chartreuse' : 'red', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								// conditonal rendering on what button should be visible base on the status of the product
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	// to fetch all products in the first render of the page.
	useEffect(()=>{
		fetchData();
	}, [])

	// [SECTION] Setting the product to Active/Inactive

	// Making the product inactive
	const archive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Making the product active
	const unarchive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/unarchive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

    const fetchUserData = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return (
					<tr key={user._id}>
						<td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{user._id}</td>
						<td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{user.firstName}</td>
						<td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{user.lastName}</td>
						<td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{user.email}</td>
                        <td style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{user.mobileNo}</td>
						<td td style={{ color: user.isAdmin ? 'chartreuse' : 'red', paddingTop: '20px', paddingBottom: '20px', textAlign: "center" }}>{user.isAdmin ? "Admin" : "Non-Admin"}</td>
					</tr>
				)
			}));
		});
	}

    useEffect(()=>{
		fetchUserData();
	}, [])

    const RemoveAdminStatus= (id, userName) =>{
		console.log(id);
		console.log(userName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
            console.log("USER DATA FOR REMOVE ADMIN IS HERE:")
			console.log(data)

			if(data){
				Swal.fire({
					title: "Admin Status Successfully Removed.",
					icon: "success",
					text: `User ${firstName} ${lastName} is no longer an Admin.`
				});
				// To show the update with the specific operation intiated.
				fetchUserData();
			}
			else{
				Swal.fire({
					title: "CANNOT REMOVE ADMIN STATUS",
					icon: "error",
					text: `Please Try Again.`
				});
			}
		})
	}

    const makeAdmin = (id, firstName) =>{
		console.log(id);
		console.log(firstName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Admin Status Granted.",
					icon: "success",
					text: `User ${firstName} ${lastName} is now an Admin.`
				});
				// To show the update with the specific operation intiated.
				fetchUserData();
			}
			else{
				Swal.fire({
					title: "CANNOT GRANT ADMIN STATUS",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// [SECTION] Adding a new product
	// Inserting a new product in our database
	const addProduct = (e) =>{
			// Prevents page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: "POST",
            headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
                    name: name,
                    description: description,
                    price: price,
                    stocks: stocks
				})
            })
            .then(res => res.json())
            .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Product succesfully Added",
                    icon: "success",
                    text: `${name} is now added`
                });

		    		// To automatically add the update in the page
                    fetchData();
		    		// Automatically closed the modal
                    closeAdd();
                }
                else{
                    Swal.fire({
                        title: "Error!",
                        icon: "error",
                        text: `Something went wrong. Please try again later!`
                    });
                    closeAdd();
                }

            })

		    // Clear input fields
            setName('');
            setDescription('');
            setPrice(0);
            setStocks(0);
	}

	// [SECTION] Edit a specific product
	// Updating a specific product in our database
	// edit a specific product
	const editProduct = (e) =>{
			// Prevents page redirection via form submission
            e.preventDefault();

            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
                method: "PATCH",
                headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
                    name: name,
                    description: description,
                    price: price,
                    stocks: stocks
				})
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(data){
                    Swal.fire({
                        title: "Product succesfully Updated",
                        icon: "success",
                        text: `${name} is now updated`
                    });

		    		// To automatically add the update in the page
                    fetchData();
		    		// Automatically closed the form
                    closeEdit();

                }
                else{
                    Swal.fire({
                        title: "Error!",
                        icon: "error",
                        text: `Something went wrong. Please try again later!`
                    });

                    closeEdit();
                }

            })

		    // Clear input fields
            setName('');
            setDescription('');
            setPrice(0);
            setStocks(0);
	} 

	// Submit button validation for add/edit product
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and stock greater than zero.
        if(name != "" && description != "" && price > 0 && stocks > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price, stocks]);



	return(
		(user.isAdmin == true)
		?
		<>
			{/*Header for the admin dashboard and functionality for create product and show enrollments*/}
			<div className="mt-5 mb-3 text-center">
				<h1 style={{ color: 'white', paddingTop: '20px'}}>Admin Dashboard</h1>
				{/*Adding a new product */}
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Product</Button>
				{/*To view all the user enrollments*/}
			</div>
			{/*End of admin dashboard header*/}

			{/*For view all the products in the database.*/}
			<Table striped bordered hover style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px'}}>
            <thead>
            <tr>
            <th className = "text">Product ID</th>
            <th className = "text">Product Name</th>
            <th className = "text">Description</th>
            <th className = "text">Price</th>
            <th className = "text">Stocks</th>
            <th className = "text">Status</th>
            <th className = "text">Actions</th>
                </tr>
                    </thead>
                    <tbody>
                    {allProducts}
                    </tbody>
                </Table>
			{/*End of table for product viewing*/}
            <Table striped bordered hover style={{ color: 'white', paddingTop: '20px', paddingBottom: '20px'}}>
            <thead>
            <tr>
            <th className = "text">User ID</th>
            <th className = "text">First Name</th>
            <th className = "text">Last Name</th>
            <th className = "text">E-mail address</th>
            <th className = "text">Mobile Number</th>
            <th className = "text">isAdmin?</th>
                </tr>
                    </thead>
                    <tbody>
                    {allUsers}
                    </tbody>
                </Table>
            {/*Modal for Adding a new product*/}
            <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
            <Form onSubmit={e => addProduct(e)}>

                    <Modal.Header closeButton>
                        <Modal.Title>Add New Product</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <Form.Group controlId="name" className="mb-3">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="Enter Product Name" 
                                value = {name}
                                onChange={e => setName(e.target.value)}
                                required
                                />
                        </Form.Group>

                        <Form.Group controlId="description" className="mb-3">
                            <Form.Label>Product Description</Form.Label>
                            <Form.Control
                                as="textarea"
                                rows={3}
                                placeholder="Enter Product Description" 
                                value = {description}
                                onChange={e => setDescription(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="price" className="mb-3">
                            <Form.Label>Product Price</Form.Label>
                            <Form.Control 
                                type="number" 
                                placeholder="Enter Product Price" 
                                value = {price}
                                onChange={e => setPrice(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="stocks" className="mb-3">
                            <Form.Label>Product Stocks</Form.Label>
                            <Form.Control 
                                type="number" 
                                placeholder="Enter Product Stocks" 
                                value = {stocks}
                                onChange={e => setStocks(e.target.value)}
                                required
                                />
                        </Form.Group>
                        </Modal.Body>

                <Modal.Footer>
                    { isActive 
                            ? 
                            <Button variant="primary" type="submit" id="submitBtn">Save</Button>
                            : 
                            <Button variant="danger" type="submit" id="submitBtn" disabled>Save</Button>
                        }
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                </Modal.Footer>

                </Form>	
            </Modal>
            {/*End of modal for adding product*/}

            {/*Modal for Editing a product*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
        <Form onSubmit={e => editProduct(e)}>

                <Modal.Header closeButton>
                    <Modal.Title>Edit a Product</Modal.Title>
                    </Modal.Header>

                <Modal.Body>
                    <Form.Group controlId="name" className="mb-3">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter Product Name" 
                            value = {name}
                            onChange={e => setName(e.target.value)}
                            required
                            />
                    </Form.Group>

                    <Form.Group controlId="description" className="mb-3">
                        <Form.Label>Product Description</Form.Label>
                        <Form.Control
                            as="textarea"
                            rows={3}
                            placeholder="Enter Product Description" 
                            value = {description}
                            onChange={e => setDescription(e.target.value)}
                            required
                            />
                    </Form.Group>

                    <Form.Group controlId="price" className="mb-3">
                        <Form.Label>Product Price</Form.Label>
                        <Form.Control 
                            type="number" 
                            placeholder="Enter Product Price" 
                            value = {price}
                            onChange={e => setPrice(e.target.value)}
                            required
                            />
                    </Form.Group>

                    <Form.Group controlId="stocks" className="mb-3">
                        <Form.Label>Product Stocks</Form.Label>
                        <Form.Control 
                            type="number" 
                            placeholder="Enter Product Stocks" 
                            value = {stocks}
                            onChange={e => setStocks(e.target.value)}
                            required
                            />
                    </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                    { isActive 
                        ? 
                        <Button variant="primary" type="submit" id="submitBtn">Save</Button>
                        : 
                        <Button variant="danger" type="submit" id="submitBtn" disabled>Save</Button>
                    }
                    <Button variant="secondary" onClick={closeEdit}>Close</Button>
                </Modal.Footer>

            </Form>	
        </Modal>
        {/*End of modal for editing a product*/}
		</>
		:
		<Navigate to="/products" />
	)
}