import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights' ;
import BannerCSS from "../BannerCSS.css";


export default function Home() {

	const data = {
		title: "Morante Capston3 E-Commerce Store",
		content: "HOMEPAGE PLACEHOLDER",
		destination: "/products",
		label: "Browse Products Now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}