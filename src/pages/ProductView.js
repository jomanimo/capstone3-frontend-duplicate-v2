import { useState, useEffect, useContext } from "react";

import {Container, Card, Button, Row, Col, Form} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function ProductView(){

	const { user } = useContext(UserContext);

	const { productId} = useParams();
	/* 
	console.log("STOCKS HERE")
	console.log(stocks) */

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1); // added state for quantity
	const [stocks, setStocks] = useState(0);

	const [showFullDescription, setShowFullDescription] = useState(0)


	useEffect(()=>{
		console.log(productId);
		console.log("THIS IS THE QUANTITY:")
		console.log(quantity);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => {
		// console.log("RES.JSON")
		// console.log(res.json);
		return	res.json()})
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);

		});

	}, [productId])

	const purchase = (productId) =>{
		console.log("PARAMETER:")
		console.log(productId, quantity, localStorage.getItem('token'));
		fetch(`${process.env.REACT_APP_API_URL}/products/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				_id: productId,
				quantity: quantity
			})
		})
		.then(res => {
		console.log("JSON REAL:")
		console.log(res.json);
		return res.json()})
		.then(data => {

			console.log("Order data: ")
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully Purchased!",
					icon: "success",
					text: "You have successfully purchased this item."
				});

				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return(
		<Container style={{textAlign: "center"}}>
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>
				{showFullDescription
                ? description
                : `${description.substring(0, 100)}...`}
				<button onClick={() => setShowFullDescription(!showFullDescription)}>
                {showFullDescription ? 'See less' : 'See more'}
				</button>
            </Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PHP {price * quantity}</Card.Text>
							<Card.Subtitle>Stocks:</Card.Subtitle>
							<Card.Text>{stocks} available</Card.Text>
							<Form>
								<Form.Group controlId="quantitySelect">
									<Form.Label>Quantity:</Form.Label>
									<Form.Control 
										type="number"
										value={quantity}
										onChange={(e) => setQuantity(parseInt(e.target.value))}
									/>

								</Form.Group>
							</Form>
							{
								(user.id !== null)
								?
									<Button variant="primary"  size="lg" onClick={() => purchase(productId)}>Purchase</Button>
								:
									<Button as={Link} to="/login" variant="success"  size="lg">Login to Purchase</Button>
							}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
						}