import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext'  //s45
import {Navigate} from 'react-router-dom'; //s45
import LoginCSS from "../LoginCSS.css";

import Swal from 'sweetalert2';

export default function Login() {
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);
    const [id, setId] = useState('');

    

    function authenticate(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: { 'Content-Type' : 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log("Check accessToken");
            console.log(data.access);
            setId(data.access);
            if(typeof data.access !== "undefined"){
                localStorage.setItem('token' , data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Successfully Logged in. Happy Shopping!"
                })   
            }
            else{
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }


        }) 

        setEmail(''); 
    }


    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            },
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            console.log("retrieveUserDetails: ")
            console.log(data);
            localStorage.setItem("userId", data._id);
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


    useEffect(() => {
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);


    return (
        (user.id !== null)
        ?
        <Navigate to="/products" />
        :
        <div className="form-container">
        <Form onSubmit = {(e) => authenticate(e)}>
        <h3 className ="text-center" style={{ color: 'white', paddingTop: '20px'}}> Login </h3>
            <Form.Group controlId="userEmail">
                <Form.Label style = {{color: 'white'}}>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                    className = "form-input"
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label style = {{color: 'white'}}>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                    className = "form-input"
                />
            </Form.Group>

             { isActive ?  // true
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : // false
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }

        </Form>
        </div>
    )
}
